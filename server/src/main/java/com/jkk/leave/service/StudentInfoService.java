package com.jkk.leave.service;

import com.jkk.leave.entity.DO.StudentDo;
import com.jkk.leave.entity.POJO.Student;
import com.jkk.leave.entity.POJO.User;
import com.jkk.leave.entity.VO.StudentVO;

public interface StudentInfoService {
	Integer getStudentClass(Integer studentId);

	String getStudentCounselorName(Integer studentId);

	Integer getStudentCounselorId(Integer studentId);

	int insertStuInfo(StudentDo studentDo);
}
