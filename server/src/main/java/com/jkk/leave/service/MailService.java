package com.jkk.leave.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

/**
 * @author thinkpad - 83start
 * @version v1.0
 * @create 2022/8/14 3:07
 * @package com.test.springboot.mail.service
 * @description 文件说明
 */

@Component
public class MailService {
    @Autowired
    JavaMailSender mailSender;
    @Value("${spring.mail.username}")
    private String sender;

    /**
     * 发送邮件
     *
     * @param email   接收者邮箱
     * @param subject 邮件主题
     * @param content 邮件内容
     */
    public void sendMail(String email, String subject, String content) {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(sender);
            helper.setTo(email);
            helper.setSubject(subject);
            helper.setText(content);
            mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            System.out.println(e);
        }
    }
}
