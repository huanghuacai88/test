package com.jkk.leave.service;


import com.jkk.leave.entity.DO.LeaveApplyDO;
import com.jkk.leave.entity.DO.NoticeListDo;
import com.jkk.leave.entity.VO.NoticeListVO;

import java.util.List;

public interface NoticeService {


    void approveLeave(int LeaveId);

    List<NoticeListVO> notifyTeacher();

    void notifyParentByEmail(LeaveApplyDO leaveApplyDO);

    int addnotice(LeaveApplyDO leaveApplyDO);

}
