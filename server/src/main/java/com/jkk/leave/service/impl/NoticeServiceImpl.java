package com.jkk.leave.service.impl;

import com.jkk.leave.entity.DO.LeaveApplyDO;
import com.jkk.leave.entity.DO.NoticeListDo;
import com.jkk.leave.entity.DO.ParentDO;
import com.jkk.leave.entity.DO.StudentDo;
import com.jkk.leave.entity.POJO.User;
import com.jkk.leave.entity.VO.NoticeListVO;
import com.jkk.leave.entity.VO.ParentVO;
import com.jkk.leave.entity.VO.StudentVO;
import com.jkk.leave.mapper.*;
import com.jkk.leave.service.MailService;
import com.jkk.leave.service.NoticeService;


import com.jkk.leave.tools.ApplyStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NoticeServiceImpl implements NoticeService {
    @Autowired
    LeaveApplyMapper leaveApplyMapper;
    @Autowired
    StudentInfoMapper studentInfoMapper;
    @Autowired
    NoticeListMapper noticeListMapper;
    @Autowired
    ParentMapper parentMapper;
    @Autowired
    MailService mailService;
    @Autowired
    UserMapper userMapper;

    @Autowired
    private JavaMailSender mailSender; // 注入邮件发送器


    public void approveLeave(int leaveId) {
        LeaveApplyDO leaveApplyDO = leaveApplyMapper.selectByPrimaryKey(leaveId);
        if (leaveApplyDO != null) {
            int status = leaveApplyDO.getStatus();
            if (status == 2) {

                int noticeId = addnotice(leaveApplyDO);
                NoticeListDo noticeListDo = noticeListMapper.selectByPrimaryKey(noticeId);
                // 发送通知给教师
                notifyTeacher();
                // 发送通知给家长
                notifyParentByEmail(leaveApplyDO);
            }

        }
    }

    @Override
    public int addnotice(LeaveApplyDO leaveApplyDO) {
        NoticeListDo noticeListDo =
                NoticeListDo.builder()
                        .stuId(leaveApplyDO.getStudentId())
                        .leaveId(leaveApplyDO.getId())
                        .build();
        int num = noticeListMapper.insert(noticeListDo);
        if (num == 1) {
            return noticeListDo.getId();
        } else {
            return num;
        }
    }

    @Override
    public List<NoticeListVO> notifyTeacher() {
        List<NoticeListVO> noticeListVOList = new ArrayList<>();
        List<NoticeListDo> noticeListDoList = noticeListMapper.selectAll();
        for (NoticeListDo noticeListDo : noticeListDoList) {


            User userInfoById = userMapper.getUserInfoById(noticeListDo.getStuId());
            NoticeListVO noticeListVO = NoticeListVO.builder().name(userInfoById.getName()).type(String.valueOf(userInfoById.getType())).build();
            noticeListVOList.add(noticeListVO);

        }


        return noticeListVOList;


    }

    @Override
    public void notifyParentByEmail(LeaveApplyDO leaveApplyDO) {
//         根据学生ID获取家长邮箱，这里假设你有一个方法 getParentEmailById
//   leave applydo包含申请id =28 得到stu_id=1,
//      leaveApplyMapper.selectByPrimaryKey(leaveApplyDO.());
        int sid = leaveApplyDO.getStudentId();
        StudentDo studentDo = studentInfoMapper.selectByPrimaryKey(sid);
        if (studentDo == null) {
            throw new IllegalArgumentException("Student not found with ID: " + leaveApplyDO.getStudentId());
        }
//原来
//        ParentDO parentDO = parentMapper.selectByPrimaryKey(studentDo.getParentId());

        User user=userMapper.getUserInfoById(studentDo.getParentId());
      //原来
//        String parent_email = parentDO.getParentEmail();

        String parent_email = user.getEMail();
        if (parent_email != null) {
//            SimpleMailMessage message = new SimpleMailMessage();
//            message.setFrom("no-reply@moonshot.com");
//            message.setTo(parentEmail);
            String subject = "您的孩子的请假申请已批准";
            String content = "您好，\n\n我们想通知您，您的孩子 " + " 的请假申请已被批准。请假详情如下：\n类型：" + leaveApplyDO.getType() + "\n开始日期：" + leaveApplyDO.getStartTime() + "\n结束日期：" + leaveApplyDO.getEndTime() + "\n\n谢谢。";
            mailService.sendMail(parent_email, subject, content);
        }
    }

}