package com.jkk.leave.service.impl;

import com.jkk.leave.entity.DO.LeaveApplyDO;
import com.jkk.leave.entity.DO.StudentDo;
import com.jkk.leave.entity.POJO.Student;
import com.jkk.leave.entity.POJO.User;
import com.jkk.leave.entity.VO.LeaveApplyVO;
import com.jkk.leave.entity.VO.StudentVO;
import com.jkk.leave.mapper.StudentInfoMapper;
import com.jkk.leave.service.StudentInfoService;
import com.jkk.leave.service.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentInfoServiceImpl implements StudentInfoService {
	private final StudentInfoMapper studentInfoMapper;
	private final UserService userService;


	public StudentInfoServiceImpl(StudentInfoMapper studentInfoMapper, UserService userService) {
		this.studentInfoMapper = studentInfoMapper;
		this.userService = userService;
	}

	@Override
	public Integer getStudentClass(Integer studentId) {
		return studentInfoMapper.getClass(studentId);
	}
	//需要教师id，数据库请假单不用stu_id，实现教师请假
	@Override
	public String getStudentCounselorName(Integer studentId) {
		return userService.getUserName(studentInfoMapper.getCounselorId(studentId));
	}

	@Override
	public Integer getStudentCounselorId(Integer studentId) {
		return studentInfoMapper.getCounselorId(studentId);
	}

	@Override
	public int insertStuInfo(StudentDo studentDo) {

//有问题
		return studentInfoMapper.insert(studentDo);
	}
}
