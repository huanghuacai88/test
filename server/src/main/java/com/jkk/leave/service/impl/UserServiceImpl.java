package com.jkk.leave.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.jkk.leave.entity.DO.LeaveApplyDO;
import com.jkk.leave.entity.DO.UserDo;
import com.jkk.leave.entity.POJO.User;
import com.jkk.leave.entity.VO.LeaveApplyVO;
import com.jkk.leave.entity.VO.UserVO;
import com.jkk.leave.mapper.UserMapper;
import com.jkk.leave.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

	private final UserMapper userMapper;

	public UserServiceImpl(UserMapper userMapper) {
		this.userMapper = userMapper;
	}

	@Override
	public User isLogin(User user) {
		return userMapper.isLogin(user);
	}

	@Override
	public int getTypeIdById(Integer id) {
		return userMapper.selectByPrimaryKey(id).getType();
	}
//需要教师id，数据库请假单不用stu_id，实现教师请假
	@Override
	public String getUserName(Integer id) {
		return userMapper.selectByPrimaryKey(id).getName();
	}

	public User getUserById(Integer id){
		return userMapper.selectByPrimaryKey(id);
	}

	@Override
	public int modifyPwd(Integer id, String pwd) {
		User user = new User();
		user.setId(id);
		user.setPassword(pwd);
		return userMapper.updateByPrimaryKeySelective(user);
	}

	@Override
	public int modifyAvatar(Integer id, String avatarPath) {
		User user = new User();
		user.setId(id);
		user.setAvatar(avatarPath);
		return userMapper.updateByPrimaryKeySelective(user);
	}

	@Override
	public int modifyEMail(Integer id, String eMail) {
		User user = new User();
		user.setId(id);
		user.setEMail(eMail);
		return userMapper.updateByPrimaryKeySelective(user);
	}

	@Override
	public User selectone(String name) {
		return userMapper.selectone(name);
	}

	@Override
	public int insertUser(User user) {
		return userMapper.insert(user);
	}

	@Override
	public List<UserVO> selectAllStudent() {
		List<User> users = userMapper.selectAll();
		//报错：类型不匹配：在您的代码中，可能有一个方法或组件期望接收 UserDo 类型的参数，但实际上接收到了 User 类型的参数。
		//错误的实体类使用：可能在某个地方错误地使用了 User 实体类，而应该使用 UserDo 实体类。
		List<UserVO> ret = new ArrayList<>();
		for (User userDo : users) {
			if (userDo.getType() == 0) {
				UserVO userVO =
						UserVO.builder()
								.id(userDo.getId())
								.name(userDo.getName())
								.password(userDo.getPassword())
								.eMail(userDo.getEMail())
								.avatar(userDo.getAvatar())
								.build();
				ret.add(userVO);
			}

		}

		return ret;
	}


}
