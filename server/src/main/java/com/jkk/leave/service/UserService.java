package com.jkk.leave.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.jkk.leave.entity.POJO.User;
import com.jkk.leave.entity.VO.UserVO;
import org.apache.poi.ss.formula.functions.T;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface UserService {
    User isLogin(User user);

    int getTypeIdById(Integer id);

    String getUserName(Integer id);

    User getUserById(Integer id);

    int modifyPwd(Integer id, String pwd);

    int modifyAvatar(Integer id, String avatarPath);

    int modifyEMail(Integer id, String eMail);

    User selectone(String name);

    int insertUser(User user);

List<UserVO> selectAllStudent();
}
