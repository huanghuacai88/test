package com.jkk.leave.config;

import com.jkk.leave.handler.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcInterceptorConfig implements WebMvcConfigurer {
	private final StudentApplyInterceptor studentApplyInterceptor;
	private final CounselorApplyInterceptor counselorApplyInterceptor;
	private final CollegeApplyInterceptor collegeApplyInterceptor;
	private final ParentInterceptor parentInterceptor;
	private final TeacherApplyInterceptor teacherApplyInterceptor;
	private final LoginInterceptor loginInterceptor;
	public MvcInterceptorConfig(StudentApplyInterceptor studentApplyInterceptor, CounselorApplyInterceptor counselorApplyInterceptor, CollegeApplyInterceptor collegeApplyInterceptor, ParentInterceptor parentInterceptor, TeacherApplyInterceptor teacherApplyInterceptor, LoginInterceptor loginInterceptor) {
		this.studentApplyInterceptor = studentApplyInterceptor;
		this.counselorApplyInterceptor = counselorApplyInterceptor;
		this.collegeApplyInterceptor = collegeApplyInterceptor;
		this.parentInterceptor = parentInterceptor;
		this.teacherApplyInterceptor = teacherApplyInterceptor;
		this.loginInterceptor = loginInterceptor;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
//		当匹配到/** 匹配到所有路径，就会拦截下来，交给login拦截器，
//		当用户没登录的时候，就会拦截下来，否则就会请求到后续的controller


		registry.addInterceptor(loginInterceptor)
				.addPathPatterns("/**")
				.excludePathPatterns("/user/**")
				.excludePathPatterns("/user/login")
				.excludePathPatterns("/user/register")

				.excludePathPatterns("/file/**")
				.excludePathPatterns("/img/**")
				.excludePathPatterns("/static/**")
				.excludePathPatterns("/test/**")

				.excludePathPatterns("/upload/**");

//		registry.addInterceptor(studentApplyInterceptor)
//				.addPathPatterns("/stu/**");
		registry.addInterceptor(counselorApplyInterceptor)
				.addPathPatterns("/cou/**");
		registry.addInterceptor(collegeApplyInterceptor)
				.addPathPatterns("/col/**");
		registry.addInterceptor(teacherApplyInterceptor)
				.addPathPatterns("/tea/**")
				.excludePathPatterns("/tea/list");
		registry.addInterceptor(parentInterceptor)
				.addPathPatterns("/par/**");
	}

//	@Value("${prop.img}")
//	private String IMG_FOLDER;
//	@Override
//	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//		registry.addResourceHandler("/img/**").addResourceLocations("file:" + IMG_FOLDER);
//	}
}
