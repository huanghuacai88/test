package com.jkk.leave.web.api.v1;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.jkk.leave.entity.DO.StudentDo;
import com.jkk.leave.entity.POJO.Student;
import com.jkk.leave.entity.POJO.User;
import com.jkk.leave.entity.VO.LeaveApplyVO;
import com.jkk.leave.entity.VO.UserVO;
import com.jkk.leave.mapper.UserMapper;
import com.jkk.leave.service.StudentInfoService;
import com.jkk.leave.service.UserService;
import com.jkk.leave.tools.DataTool;
import com.jkk.leave.utils.RestfulRes;
import com.jkk.leave.utils.UUIDUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    UserMapper userMapper;
    @Autowired
    StudentInfoService studentInfoService;
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("login")
    public RestfulRes login(String id, String password, HttpSession session) {
        User user = new User();

        try {
            user.setId(Integer.parseInt(id));
        } catch (NumberFormatException e) {
            return RestfulRes.fail("账号只能是数字");
        }

        user.setPassword(password);
        User res = userService.isLogin(user);
        if (res != null) {
            session.setAttribute("user", res);
            return RestfulRes.success();
        } else {
            return RestfulRes.fail("账号或密码错误");
        }

    }

    /**
     * 注册
     */

    @PostMapping(value = "/register")
    public RestfulRes register(String id, String password, String name, Integer type, String e_mail, HttpSession session) {
//    	ValidatorUtils.validateEntity(user);
        User user = new User();
        user.setId(Integer.parseInt(id));
        user.setPassword(password);
        user.setName(name);
        user.setType(type);
        user.setEMail(e_mail);
        if (userService.selectone(user.getName()) != null) {
            return RestfulRes.fail("用户已存在");
        }
        userService.insertUser(user);
        StudentDo studentDo =
                StudentDo.builder()
                        .id(Integer.parseInt(id))
                        .classId(10022)
                        .name(name)
//                .area(area)
                        .parentId(11)
                        .build();
        studentInfoService.insertStuInfo(studentDo);
//有问题
        //单单插入了user表，但是学生信息表并没有新增信息
//        studentInfoService.insertStuInfo(user);
//有问题
        return RestfulRes.success("注册成功");
    }

    @GetMapping("logout")
    public RestfulRes logout(HttpSession session) {
        session.invalidate();
        return RestfulRes.success();
    }


    @GetMapping("/selectAllStu")
    public RestfulRes<List<UserVO>> selectAllStu() {

        List<UserVO> students = userService.selectAllStudent();
        return RestfulRes.success(students);

    }

    @PostMapping("/add")
    public RestfulRes addUser(@RequestBody User user) {
        // 检查用户类型是否为学生、教师或家长
        if (user.getType() == 0) { // 假设1代表学生
            // 执行学生用户的插入逻辑
            // 假设以下代码是插入学生的逻辑
            // user.setStudentId(UUIDUtil.getUUID());
            // userMapper.insertSelective(user);
            userMapper.insertSelective(user);
            return RestfulRes.success("用户添加成功");
        } else if (user.getType() == 3) { // 假设2代表教师
            // 执行教师用户的插入逻辑
            userMapper.insertSelective(user);
            return RestfulRes.success("用户添加成功");
        } else if (user.getType() == 4) { // 假设3代表家长
            // 执行家长用户的插入逻辑
            userMapper.insertSelective(user);
            return RestfulRes.success("用户添加成功");
        } else {
            // 返回错误信息，用户类型不正确
            return RestfulRes.fail("无效的用户类型");
        }

//        // 调用 MyBatis 映射器的 insertSelective 方法插入用户
//        int result = userMapper.insertSelective(user);
//        if (result > 0) {
//            // 返回成功信息
//            return RestfulRes.success("用户添加成功");
//        } else {
//            // 返回错误信息，添加失败
//            return RestfulRes.fail("用户添加失败");
//        }
    }

    @PostMapping("getInfo")
    public RestfulRes<User> getInfo(@SessionAttribute("user") User user) {
        return RestfulRes.success(userService.getUserById(user.getId()));
    }

    @PostMapping("modifyPwd")
    public RestfulRes modifyPwd(String pwd, @SessionAttribute("user") User user) {
        if (DataTool.effectivePwd(pwd) && userService.modifyPwd(user.getId(), pwd) == 1) {
            return RestfulRes.success();
        } else {
            return RestfulRes.fail("修改密码失败");
        }
    }

    @PostMapping("modifyEMail")
    public RestfulRes modifyEMail(String email, @SessionAttribute("user") User user) {
        if (DataTool.effectiveEmail(email) && userService.modifyEMail(user.getId(), email) == 1) {
            return RestfulRes.success();
        } else {
            return RestfulRes.fail("修改邮箱失败");
        }
    }

    @Value("${prop.img}")
    private String filePath;

    @PostMapping("modifyAvatar")
    public RestfulRes modifyAvatar(@RequestParam("file") MultipartFile file, @SessionAttribute("user") User user) {
        if (file.isEmpty()) {
            return RestfulRes.fail("文件不能为空");
        }
        String fileName = UUIDUtil.getUUID();

        File dest = new File(filePath + fileName);
        try {
            file.transferTo(dest);
            if (userService.modifyAvatar(user.getId(), fileName) == 1) {
                return RestfulRes.success(fileName);
            } else {
                return RestfulRes.fail("上传文件失败");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return RestfulRes.fail("上传文件失败");
        }
    }
}
