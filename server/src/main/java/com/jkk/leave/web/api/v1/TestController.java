package com.jkk.leave.web.api.v1;

import cn.hutool.core.date.DateUtil;
import com.jkk.leave.entity.POJO.User;
import com.jkk.leave.entity.POJO.base.Filter;
import com.jkk.leave.entity.POJO.base.Sorter;
import com.jkk.leave.entity.VO.ArchiveVO;
import com.jkk.leave.entity.VO.LeaveApplyVO;
import com.jkk.leave.entity.VO.WaitStatusVO;
import com.jkk.leave.service.CollegeApplyService;
import com.jkk.leave.service.LeaveApplyService;
import com.jkk.leave.service.MailService;
import com.jkk.leave.tools.DataTool;
import com.jkk.leave.tools.ExcelTool;
import com.jkk.leave.tools.FilterSorterParse;
import com.jkk.leave.tools.TimeTool;
import com.jkk.leave.utils.RestfulRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private MailService mailService;

    @GetMapping("/a")
    public String tset() throws FileNotFoundException {
        File path = new File( ResourceUtils.getURL("E:\\StudentLeave\\server\\src\\main\\resources\\static").getPath());//路径错误
        if (!path.exists()) {
            path = new File("");
        }
        File upload = new File(path.getAbsolutePath(), "upload");//
        System.out.println(upload);
        if (!upload.exists()) {
            upload.mkdirs();
        }
        String fileName = new Date().getTime() + "." + "a";
        File dest = new File(upload.getAbsolutePath() + "/" + fileName);
        return "a";


    }


}
