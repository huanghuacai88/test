package com.jkk.leave.web.api.v1;

import cn.hutool.core.date.DateUtil;
import com.jkk.leave.entity.POJO.Lesson;
import com.jkk.leave.entity.POJO.TeacherLeaveList;
import com.jkk.leave.entity.POJO.User;
import com.jkk.leave.entity.POJO.base.Filter;
import com.jkk.leave.entity.POJO.base.Sorter;
import com.jkk.leave.entity.VO.ArchiveVO;
import com.jkk.leave.entity.VO.LeaveApplyVO;
import com.jkk.leave.entity.VO.WaitStatusVO;
import com.jkk.leave.service.LeaveApplyService;
import com.jkk.leave.service.TeacherApplyService;
import com.jkk.leave.tools.DataTool;
import com.jkk.leave.tools.ExcelTool;
import com.jkk.leave.tools.FilterSorterParse;
import com.jkk.leave.tools.TimeTool;
import com.jkk.leave.utils.RestfulRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/tea")
public class TeacherLeaveController {
	private final TeacherApplyService teacherApplyService;
@Autowired
LeaveApplyService leaveApplyService;

	public TeacherLeaveController(TeacherApplyService teacherApplyService) {
		this.teacherApplyService = teacherApplyService;
	}

//	@PostMapping("list")
//	public RestfulRes<List<TeacherLeaveList>> getApplyList(@SessionAttribute("user")User user, int week, String lesson){
////		String team = TimeTool.getThisTeam();
//		String team = "2024-2026-1";
//		week += TimeTool.getWeekOfYear(team, new Date().getTime());
//		return RestfulRes.success(teacherApplyService.getApplyList(user,Integer.parseInt(team.split("-")[0]),week,lesson));
//	}
//在这加个家长区别，获取家长查看孩子请假
	/**
	 * 获取请假列表
	 *
	 * @param user   session中的用户名
	 * @param week   星期参数（可选）
	 * @param lesson 课程参数（可选）
	 * @return 请假列表
	 */
	@PostMapping("list")
	public RestfulRes<List<TeacherLeaveList>> getApplyList(@SessionAttribute("user") User user,
														   @RequestParam(value = "week", required = false) Integer week,
														   @RequestParam(value = "lesson", required = false) String lesson) {
		String team = "2024-2026-1";
		if (week != null) {
			week += TimeTool.getWeekOfYear(team, new Date().getTime());
		} else {
			// 如果没有提供week参数，则获取所有请假数据
			return RestfulRes.success(teacherApplyService.getALLApplyList(user, Integer.parseInt(team.split("-")[0])));
		}
		return RestfulRes.success(teacherApplyService.getApplyList(user, Integer.parseInt(team.split("-")[0]), week, lesson));
	}

	@GetMapping("lesson")
	public RestfulRes<List<Lesson>> getLessonList(@SessionAttribute("user")User user){
		return RestfulRes.success(teacherApplyService.getLessons(user, "2024-2026-1"));
	}

}
