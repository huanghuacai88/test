package com.jkk.leave.web.api.v1;


import com.jkk.leave.entity.DO.LeaveApplyDO;
import com.jkk.leave.entity.DO.StudentDo;
import com.jkk.leave.entity.POJO.TeacherLeaveList;
import com.jkk.leave.entity.POJO.User;
import com.jkk.leave.entity.VO.LeaveApplyVO;
import com.jkk.leave.service.LeaveApplyService;
import com.jkk.leave.service.NoticeService;
import com.jkk.leave.tools.TimeTool;
import com.jkk.leave.utils.RestfulRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/parent")
public class ParentController {

    @Autowired
    NoticeService noticeService;

    @Autowired
    LeaveApplyService leaveApplyService;

    @PostMapping("/approve/{leaveId}/{studentId}")
    public String approveLeave(@PathVariable int leaveId, @PathVariable int studentId) {
//        noticeService.approveLeave(leaveId, studentId);
        return "Leave approved and notifications sent";
    }

    @PostMapping("list")
    public RestfulRes<LeaveApplyVO> getApplyList(@SessionAttribute("user") User user,LeaveApplyDO leaveApplyDO) {

        if (user.getId()==leaveApplyDO.getStudentId()){

            return RestfulRes.success(leaveApplyService.getApplyById(user.getId(), user));

        }else {
            return RestfulRes.fail("查询失败");
        }


    }
}
