package com.jkk.leave.web.api.v1;

import com.jkk.leave.entity.POJO.User;
import com.jkk.leave.entity.POJO.base.Filter;
import com.jkk.leave.entity.POJO.base.Sorter;
import com.jkk.leave.entity.VO.LeaveApplyVO;
import com.jkk.leave.service.LeaveApplyService;
import com.jkk.leave.tools.FilterSorterParse;
import com.jkk.leave.utils.RestfulRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.util.List;

@RestController
@RequestMapping("sys")
public class SysInfoController {
	private final LeaveApplyService leaveApplyService;

	public SysInfoController(LeaveApplyService leaveApplyService) {
		this.leaveApplyService = leaveApplyService;
	}


	//修改添加
	@PostMapping
	public RestfulRes<List<LeaveApplyVO>> getLeaveApply(@SessionAttribute("user") User user, Integer page, Integer num, String custom){
		Sorter sorter = FilterSorterParse.parseSorter(custom, "sendTime");
		Filter filter = FilterSorterParse.parseFilter(custom);
		return RestfulRes.success(leaveApplyService.getApplyList(user, page, num, filter, sorter));
	}

	@PostMapping("getTeam")
	public RestfulRes<List<String>> getAllTeam(){
		return RestfulRes.success(leaveApplyService.getTeam());
	}
}
