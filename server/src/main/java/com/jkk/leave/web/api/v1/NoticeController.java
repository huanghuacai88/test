package com.jkk.leave.web.api.v1;


import com.jkk.leave.entity.DO.NoticeListDo;
import com.jkk.leave.entity.POJO.User;
import com.jkk.leave.entity.POJO.base.Filter;
import com.jkk.leave.entity.POJO.base.Sorter;
import com.jkk.leave.entity.VO.LeaveApplyVO;
import com.jkk.leave.entity.VO.NoticeListVO;
import com.jkk.leave.mapper.NoticeListMapper;
import com.jkk.leave.service.NoticeService;
import com.jkk.leave.tools.FilterSorterParse;
import com.jkk.leave.utils.RestfulRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/notice")
public class NoticeController {

    @Autowired
    NoticeService noticeService;
    @Autowired
    NoticeListMapper noticeListMapper;


    @GetMapping("/getNoticeList")
    public RestfulRes<List<NoticeListVO>> getNoticeList( ){

        return RestfulRes.success(noticeService.notifyTeacher());
    }
}
