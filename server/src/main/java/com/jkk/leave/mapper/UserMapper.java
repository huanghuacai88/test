package com.jkk.leave.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.jkk.leave.entity.DO.UserDo;
import com.jkk.leave.entity.POJO.User;
import com.jkk.leave.entity.VO.UserVO;
import com.jkk.leave.mapper.base.MyBatisBaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper extends MyBatisBaseDao<User, Integer> {
    User isLogin(User user);

    User getUserInfoById(Integer userId);

    User selectone(String name);

    List<User> selectAll();

}