package com.jkk.leave.mapper;

import com.jkk.leave.entity.DO.LeaveApplyDO;
import com.jkk.leave.entity.DO.ParentDO;
import com.jkk.leave.entity.POJO.TeacherLeaveList;
import com.jkk.leave.entity.POJO.User;
import com.jkk.leave.entity.VO.ParentVO;
import com.jkk.leave.entity.VO.StudentVO;
import com.jkk.leave.mapper.base.MyBatisBaseDao;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParentMapper extends MyBatisBaseDao<ParentDO, Integer>{
//    // 定义一个方法来获取学生信息和家长邮箱
//    ParentDO selectParentEmailByStudentId(@Param("studentId") int studentId);

    List<LeaveApplyDO> selectByParentId(User teacher, Integer year);
}
