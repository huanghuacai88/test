package com.jkk.leave.mapper;

import com.jkk.leave.entity.DO.LeaveApplyDO;
import com.jkk.leave.entity.DO.StudentDo;
import com.jkk.leave.entity.VO.StudentVO;
import com.jkk.leave.mapper.base.MyBatisBaseDao;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentInfoMapper extends MyBatisBaseDao<StudentDo, Integer>{
	Integer getCounselorId(@Param("studentId") Integer studentId);

	Integer getClass(@Param("studentId") Integer studentId);



}
