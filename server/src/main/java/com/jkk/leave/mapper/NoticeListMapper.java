package com.jkk.leave.mapper;

import com.jkk.leave.entity.DO.NoticeListDo;
import com.jkk.leave.entity.DO.ParentDO;
import com.jkk.leave.mapper.base.MyBatisBaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoticeListMapper extends MyBatisBaseDao<NoticeListDo, Integer>{
//    // 定义一个方法来获取学生信息和家长邮箱
//    ParentDO selectParentEmailByStudentId(@Param("studentId") int studentId);
   List<NoticeListDo> selectAll();
}
