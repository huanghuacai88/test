package com.jkk.leave.mapper;

import com.jkk.leave.entity.DO.StudentDo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentMangerMapper {
    List<StudentDo> findAll();


    int insert(StudentDo studentDo);

    int deleteByUid(@Param("id") String uid);

    int update(StudentDo studentDo);
}