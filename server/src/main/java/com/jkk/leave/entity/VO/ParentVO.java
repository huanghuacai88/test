package com.jkk.leave.entity.VO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ParentVO {

        private Integer id;

        private String parentName;


        private String parentEmail;




}
