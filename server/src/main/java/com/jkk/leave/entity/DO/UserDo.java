package com.jkk.leave.entity.DO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

//想对学生人员进行管理的，增删改查
@Data
@Builder
public class UserDo {
	@JsonIgnore
	private Integer id;

	private String name;

	@JsonIgnore
	private String password;


	private Integer type;

	private String avatar;

	private String eMail;
}
