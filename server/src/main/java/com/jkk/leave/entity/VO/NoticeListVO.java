package com.jkk.leave.entity.VO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
//新增
public class NoticeListVO {
	private Integer id;
	private String name;
//	private Integer stuId;
//
//	private Integer leaveId;
   private Long startTime;
	private Long endTime;
	private String type;
	private String detail;
}
