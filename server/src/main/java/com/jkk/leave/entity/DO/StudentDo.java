package com.jkk.leave.entity.DO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.relational.core.sql.In;

//想对学生人员进行管理的，增删改查
@Data
@Builder
public class StudentDo {
	private Integer id;
	private Integer classId; // 确保字段名与 MyBatis 映射文件中的一致
	private String area;
	private Integer parentId;
	private String name;
}
