package com.jkk.leave.entity.VO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class UserVO {

//        @JsonIgnore
        private Integer id;

        private String name;

//        @JsonIgnore
        private String password;


        private Integer type;

        private String avatar;

        private String eMail;




}
