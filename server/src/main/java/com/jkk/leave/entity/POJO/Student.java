package com.jkk.leave.entity.POJO;


import lombok.Data;

@Data
public class Student {
    private Integer id;
    private Integer classId;
    private String area;
    private Integer parentId;
    private String name;
}
