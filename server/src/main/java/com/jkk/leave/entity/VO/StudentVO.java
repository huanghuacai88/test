package com.jkk.leave.entity.VO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.relational.core.sql.In;

@Data
@Builder
@AllArgsConstructor
//新增
public class StudentVO {
	private Integer id;
	private Integer classId;
	private String area;
	private Integer parentId;
	private String name;
}
