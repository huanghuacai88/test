package com.jkk.leave.entity.POJO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class Parent {

    private Integer id;

    private String parentName;


    private String parentEmail;


}
