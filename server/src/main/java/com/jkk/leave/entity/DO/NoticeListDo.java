package com.jkk.leave.entity.DO;

import lombok.Builder;
import lombok.Data;

//想对学生人员进行管理的，增删改查
@Data
@Builder
public class NoticeListDo {
	private Integer id;
	private Integer stuId;

	private Integer leaveId;
}
