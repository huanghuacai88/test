package com.jkk.leave.entity.DO;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ParentDO {


        private Integer id;

        private String parentName;


        private String parentEmail;



}
