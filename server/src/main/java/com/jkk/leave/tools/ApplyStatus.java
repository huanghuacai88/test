package com.jkk.leave.tools;

public enum ApplyStatus {
	//增加一个打回补交申请材料的状态
	UN_SEND(0), WAIT(1), AGREE(2), REJECT(3),REVIEW(4)
	;

	private final Integer status;
	ApplyStatus(int status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}
}
